import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { BienvenidoComponent } from './pages/bienvenido/bienvenido.component';
import { LoginComponent } from './shared/login/login.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'Sistemas-unsch',
    component: PagesComponent,
    children:
      [
        {
          path: 'Nosotros',
          loadChildren: './pages/nosotros/nosotros.module#NosotrosModule'
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
