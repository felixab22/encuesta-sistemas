import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NosotrosRoutingModule } from './nosotros-routing.module';
import { InicioComponent } from './inicio/inicio.component';
import { SegundoComponent } from './segundo/segundo.component';

@NgModule({
  declarations: [
    InicioComponent,
    SegundoComponent
  ],
  imports: [
    CommonModule,
    NosotrosRoutingModule
  ],
  exports: [
    InicioComponent
  ],
})
export class NosotrosModule { }
