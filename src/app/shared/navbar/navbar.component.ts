import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  urlimg = '../../../assets/img/avatars/Velazquez.jpg'

  constructor(
    private _router: Router,
  ) { }

  ngOnInit() {
  }
  cerrarseccion() {
    console.log('hola');
    localStorage.removeItem('usertoken');
    localStorage.removeItem('personal');
    localStorage.removeItem('hoy');
    localStorage.removeItem('autorization');
    this._router.navigate(['']);
  }
}
